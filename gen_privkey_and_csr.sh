#!/bin/bash
#
# The MIT License (MIT)
#
# Copyright (c) 2016 Paolo Smiraglia <paolo.smiraglia@polito.it>
#                    TORSEC Group (http://security.polito.it)
#                    Politecnico di Torino
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

SSLCONF="../../openssl.cnf"
ECCURVE="secp384r1"
DIGEST="sha512"

function gen_saml_sign_csr() {
    openssl ecparam -out ec.pem -outform PEM -name $ECCURVE
    openssl req -new \
        -newkey ec:ec.pem -nodes -keyform PEM -keyout $PREFIX-privkey.pem \
        -$DIGEST -outform PEM -out $PREFIX-csr.pem \
        -subj "/C=$C/O=$O/OU=$OU/CN=$CN" \
        -config $SSLCONF -reqexts $EXTS
    #openssl req -in $PREFIX-csr.pem -noout -text
    openssl req -x509 -$DIGEST -days 3650 \
        -key $PREFIX-privkey.pem -in $PREFIX-csr.pem -out $PREFIX-cert.pem \
        -subj "/C=$C/O=$O/OU=$OU/CN=$CN" \
        -config $SSLCONF -extensions $EXTS
}

function gen_saml_enc_csr() {
    openssl req -new \
        -newkey rsa:4096 -nodes -keyform PEM -keyout $PREFIX-privkey.pem \
        -$DIGEST -outform PEM -out $PREFIX-csr.pem \
        -subj "/C=$C/O=$O/OU=$OU/CN=$CN" \
        -config $SSLCONF -reqexts $EXTS
    #openssl req -in $PREFIX-csr.pem -noout -text
    openssl req -x509 -$DIGEST -days 3650 \
        -key $PREFIX-privkey.pem -in $PREFIX-csr.pem -out $PREFIX-cert.pem \
        -subj "/C=$C/O=$O/OU=$OU/CN=$CN" \
        -config $SSLCONF -extensions $EXTS
}

function gen_tls_csr() {
    openssl ecparam -out ec.pem -outform PEM -name $ECCURVE
    openssl req -new -newkey ec:ec.pem -nodes -keyform PEM -keyout $PREFIX-privkey.pem \
        -$DIGEST -outform PEM -out $PREFIX-csr.pem \
        -subj "/C=$C/ST=$ST/L=$L/$SA/$PC/O=$O/OU=$OU/$BC/$JC/$JST/$JL/$SN/CN=$CN" \
        -config $SSLCONF -reqexts eidas_tls
    #openssl req -in $PREFIX-csr.pem -noout -text
    openssl req -x509 -$DIGEST -days 3650 \
        -key $PREFIX-privkey.pem -in $PREFIX-csr.pem -out $PREFIX-cert.pem \
        -subj "/C=$C/ST=$ST/L=$L/$SA/$PC/O=$O/OU=$OU/$BC/$JC/$JST/$JL/$SN/CN=$CN" \
        -config $SSLCONF -extensions eidas_tls
}

C="IT"
ST="Italy"
L="Torino"
O="Politecnico di Torino"
OU="eIDAS Test Infrastructure"

mkdir -p saml_enc/connector
pushd saml_enc/connector
    PREFIX="pa-connector-saml_enc"
    CN="Italian eIDAS NodeConnector PA SAML Encryption"
    EXTS="eidas_enc"
    gen_saml_enc_csr
    PREFIX="co-connector-saml_enc"
    CN="Italian eIDAS NodeConnector CO SAML Encryption"
    EXTS="eidas_enc"
    gen_saml_enc_csr
popd

mkdir -p saml_enc/proxyservice
pushd saml_enc/proxyservice
    PREFIX="proxyservice-saml_enc"
    CN="Italian eIDAS NodeProxyService SAML Encryption"
    EXTS="eidas_enc"
    gen_saml_enc_csr
popd

mkdir -p saml_sign/connector
pushd saml_sign/connector
    PREFIX="pa-connector-saml_sign"
    CN="Italian eIDAS NodeConnector PA SAML Signature"
    EXTS="eidas_sign"
    gen_saml_sign_csr
    PREFIX="co-connector-saml_sign"
    CN="Italian eIDAS NodeConnector CO SAML Signature"
    EXTS="eidas_sign"
    gen_saml_sign_csr
popd

mkdir -p saml_sign/proxyservice
pushd saml_sign/proxyservice
    PREFIX="proxyservice-saml_sign"
    CN="Italian eIDAS NodeProxyService SAML Signature"
    EXTS="eidas_sign"
    gen_saml_sign_csr
popd

mkdir -p saml_metadata_sign/connector
pushd saml_metadata_sign/connector
    PREFIX="pa-connector-saml_metadata_sign"
    CN="Italian eIDAS NodeConnector PA SAML Metadata Signature"
    EXTS="eidas_sign"
    gen_saml_sign_csr
    PREFIX="co-connector-saml_metadata_sign"
    CN="Italian eIDAS NodeConnector CO SAML Metadata Signature"
    EXTS="eidas_sign"
    gen_saml_sign_csr
popd

mkdir -p saml_metadata_sign/proxyservice
pushd saml_metadata_sign/proxyservice
    PREFIX="proxyservice-saml_metadata_sign"
    CN="Italian eIDAS NodeProxyService SAML Metadata Signature"
    EXTS="eidas_sign"
    gen_saml_sign_csr
popd

### change it according to your organisation
BC="businessCategory=Government Entity"
JC="jurisdictionCountryName=IT"
JST="jurisdictionStateOrProvinceName=Italy"
JL="jurisdictionLocalityName=Torino"
SA="streetAddress=Corso Duca degli Abruzzi 24"
PC="postalCode=10129"
###

mkdir -p tls/connector
pushd tls/connector
    SN="serialNumber=$RANDOM$RANDOM"
    CN="connector.test.eid.gov.it"
    PREFIX="connector.test.eid.gov.it"
    gen_tls_csr
    SN="serialNumber=$RANDOM$RANDOM"
    CN="com-connector.test.eid.gov.it"
    PREFIX="com-connector.test.eid.gov.it"
    gen_tls_csr
popd

mkdir -p tls/proxyservice
pushd tls/proxyservice
    SN="serialNumber=$RANDOM$RANDOM"
    CN="service.test.eid.gov.it"
    PREFIX="service.test.eid.gov.it"
    gen_tls_csr
popd

